﻿using System;
using ExampleOfGame.GameEngine;

namespace ExampleOfGame
{
    class Program
    {
        static void Main(string[] args)
        {
            new Engine().Run();
        }
    }
}
