﻿using ExampleOfGame.GameEngine.Interfaces;
using ExampleOfGame.GameEngine.Models;
using ExampleOfGame.GameEngine.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Knights
{
    class DominikKnight : IKnight
    {
        private KnightModel _knightModel;

        public DominikKnight()
        {
            _knightModel = new KnightModel
            {
                Name = "Dominik",
                Weapn = new SwordWeapon()
            };
        }

        public string GetName()
        {
            return _knightModel.Name;

        }
    }
}
//asdfasd