﻿using ExampleOfGame.GameEngine.Interfaces;
using ExampleOfGame.GameEngine.Models;
using ExampleOfGame.GameEngine.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Knights
{
    class MarcinKnight : IKnight
    {
        private KnightModel _knightModel;
        public MarcinKnight()
        {
            _knightModel = new KnightModel
            {
                Name = "Marcin",
                Weapn = new AxeWeapon(Booster.Attack)
            };
        }


        public string GetName()
        {
            return _knightModel.Name;
        }
    }
}
