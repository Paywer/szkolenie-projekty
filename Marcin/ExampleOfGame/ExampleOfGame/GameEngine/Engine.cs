﻿using ExampleOfGame.GameEngine.Interfaces;
using ExampleOfGame.GameEngine.Knights;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine
{
    public class Engine
    {

        public void Run()
        {
            Fight(new MarcinKnight(), new DominikKnight());
        }

        public void Fight(IKnight knight1, IKnight knight2)
        {
            Console.WriteLine($"{knight1.GetName()} walczy z {knight2.GetName()}");
        }

    }
}
