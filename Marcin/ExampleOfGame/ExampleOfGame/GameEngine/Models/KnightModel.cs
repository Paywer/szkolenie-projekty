﻿using ExampleOfGame.GameEngine.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Models
{
    class KnightModel
    {
        public string Name { get; set; }
        public IWeapon Weapn { get; set; }
    }
}
