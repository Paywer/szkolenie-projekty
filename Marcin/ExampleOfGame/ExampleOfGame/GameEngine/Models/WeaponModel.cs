﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Models
{
    class WeaponModel
    {
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Defeance { get; set; }
    }
}
