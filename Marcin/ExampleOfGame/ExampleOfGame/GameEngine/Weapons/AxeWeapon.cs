﻿using ExampleOfGame.GameEngine.Interfaces;
using ExampleOfGame.GameEngine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Weapons
{
    class AxeWeapon : IWeapon
    {
        private WeaponModel _weaponModel;
        private Booster _booster;

        public AxeWeapon(Booster booster)
        {
            _booster = booster;
            _weaponModel = new WeaponModel()
            {
                Name = "Axe",
                Attack = 40,
                Defeance = 10
            };
        }

        public int GetAttack()
        {
            return _booster == Booster.Attack ? _weaponModel.Attack * 2 : _weaponModel.Attack;
        }

        public int GetDefence()
        {
            return _booster == Booster.Defence ? _weaponModel.Defeance * 2 : _weaponModel.Defeance;
        }

        public string GetName()
        {
            return _weaponModel.Name;
        }
    }

    enum Booster
    {
        Attack,
        Defence
    }
}
