﻿using ExampleOfGame.GameEngine.Interfaces;
using ExampleOfGame.GameEngine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Weapons
{
    class SwordWeapon : IWeapon
    {
        private WeaponModel _weaponModel;

        public SwordWeapon()
        {
            _weaponModel = new WeaponModel()
            {
                Name = "Sword",
                Attack = 30,
                Defeance = 20
            };
        }

        public int GetAttack()
        {
            return _weaponModel.Attack;
        }

        public int GetDefence()
        {
            return _weaponModel.Defeance;
        }

        public string GetName()
        {
            return _weaponModel.Name;
        }
    }
}
