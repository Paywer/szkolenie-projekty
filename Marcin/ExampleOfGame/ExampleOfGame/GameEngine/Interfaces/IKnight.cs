﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Interfaces
{
    public interface IKnight
    {
        string GetName();
    }
}
