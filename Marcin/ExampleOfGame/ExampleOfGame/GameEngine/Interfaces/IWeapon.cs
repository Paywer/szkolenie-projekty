﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleOfGame.GameEngine.Interfaces
{
    public interface IWeapon
    {
        string GetName();
        int GetDefence();
        int GetAttack();
    }
}
