﻿using System;

public class Rycerz
{
	public readonly string name;
	public int zdrowie, sila;
	public Bron bron;

	public Rycerz(string name, int zdrowie, int sila)
	{
		this.name = name;
		this.zdrowie = zdrowie;
		this.sila = sila;
	}
}
