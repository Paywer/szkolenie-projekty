using System;
using System.Collections.Generic;
using System.Linq;

namespace Arena_walk
{
    class Program
    {
        static void Main(string[] args)
        {
            //-------------Generowanie obiektóww--------------------------------------------------------------------------           
            Rycerz R1 = new Rycerz("Paywer", 100, 10);
            Rycerz R2 = new Rycerz("MarZator", 100, 6);
            Rycerz R3 = new Rycerz("SwordMaster", 100, 4);
            Bron B1 = new Bron("Miecz", 15, 35, 10, 25, true, 15);
            Bron B2 = new Bron("Buława", 10, 40, 5, 20, true, 10);
            Bron B3 = new Bron("Dzida", 5, 25, 0, 10, true, 20);
            Bron B4 = new Bron("Topór", 20, 40, 0, 10, false, 0);
            Bron B5 = new Bron("Miecz i tarcza", 15, 35, 20, 40, false, 0);

            List<Rycerz> rycerze = new List<Rycerz> { R1, R2, R3 };
            List<Bron> bronie = new List<Bron> { B1, B2, B3, B4, B5 };

            bronie = LosowanieCzestotliwosciLeczeniaBroniAll(bronie);
            rycerze = LosowanieBroniDlaRycerzy(rycerze, bronie);
            List<Rycerz> walczacy = LosowanieWalczacych(rycerze);

            //-----------------------Przebieg walki----------------------------------------------------------------------------------------------------------
            Console.WriteLine("Konsolowa arena walk! \n");
            WaitForKey();
            Console.Clear();

            Console.WriteLine("Wylosowani przeciwnicy: \n");
            WypiszGraczy(walczacy);
            WaitForKey();
            walczacy = Walka(walczacy);
            WypiszWynikiKoncowe(walczacy);
            WaitForKeyEnd();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------METODY----------------------------------------------------------------------------------
        private static void WypiszWynikiKoncowe(List<Rycerz> walczacy)
        {
            Console.WriteLine("___________________________________________________________________________\n");
            Console.WriteLine("---Koniec walki---\n");

            if (walczacy[0].zdrowie <= 0)
            {
                Console.WriteLine($"Zwycięzca: {walczacy[1].name}. Poziom zdrowia: {walczacy[1].zdrowie}\n");
                Console.WriteLine($"Przegrany: {walczacy[0].name}. Poziom zdrowia: {walczacy[0].zdrowie}\n");
            }
            else
            {
                Console.WriteLine($"Zwycięzca: {walczacy[0].name}. Poziom zdrowia: {walczacy[0].zdrowie}\n");
                Console.WriteLine($"Przegrany: {walczacy[1].name}. Poziom zdrowia: {walczacy[1].zdrowie}\n");

            }
        }

        private static List<Rycerz> Walka(List<Rycerz> walczacy)
        {
            int runda = 1;
            Random rnd = new Random();
            while (walczacy[0].zdrowie > 0 && walczacy[1].zdrowie > 0)
            {
                int Atak_1 = (rnd.Next(walczacy[1].bron.atak_min, walczacy[1].bron.atak_max + 1) + walczacy[1].sila - rnd.Next(walczacy[0].bron.obrona_min, walczacy[0].bron.obrona_max + 1));
                int Atak_2 = (rnd.Next(walczacy[0].bron.atak_min, walczacy[0].bron.atak_max + 1) + walczacy[0].sila - rnd.Next(walczacy[1].bron.obrona_min, walczacy[1].bron.obrona_max + 1));

                if (Atak_1 > 0)
                    walczacy[0].zdrowie -= Atak_1;
                if (Atak_2 > 0)
                    walczacy[1].zdrowie -= Atak_2;

                WypiszWynikiRundy(walczacy, runda);
                walczacy = Leczenie(walczacy);
                runda++;
            }
            return walczacy;
        }

        private static List<Rycerz> Leczenie(List<Rycerz> walczacy)
        {
            if (walczacy[0].zdrowie > 0 && walczacy[1].zdrowie > 0)
            {
                Boolean write = false;
                foreach (var rycerz in walczacy)
                {
                    if (rycerz.bron.leczenie == true)
                    {
                        rycerz.bron.licznik++;
                        if (rycerz.bron.licznik == rycerz.bron.tura)
                        {
                            if (write == false)
                            {
                                Console.WriteLine("___________________________________________________________________________\n");
                                Console.WriteLine("Leczenie \n");
                                write = true;
                            }
                            Console.WriteLine($"Rycerz: {rycerz.name}   Zdrowie: {rycerz.zdrowie} + {rycerz.bron.ilosc_zdrowia} \n");
                            rycerz.zdrowie += rycerz.bron.ilosc_zdrowia;
                            //rycerz.bron.LosowanieCzestotliwosciLeczenia();
                            rycerz.bron.licznik = 0;
                        }
                    }
                }
                WaitForKey();
            }
            return walczacy;
        }

        private static void WypiszWynikiRundy(List<Rycerz> walczacy, int runda)
        {
            Console.WriteLine("___________________________________________________________________________\n");
            Console.WriteLine($"Runda {runda} \n");
            int x = 1;
            foreach (var rycerz in walczacy)
            {
                Console.WriteLine($"Rycerz {x}: {rycerz.name}   Zdrowie: {rycerz.zdrowie} \n");
                x++;
            }
        }

        private static List<Rycerz> LosowanieWalczacych(List<Rycerz> rycerze)
        {
            Random rnd = new Random();
            int a = rnd.Next(0, 3);
            int b = rnd.Next(0, 3);
            Rycerz[] walczacy = new Rycerz[2];

            do
            {
                walczacy[0] = rycerze[a];
                walczacy[1] = rycerze[b];
            }
            while (a == b);

            return walczacy.ToList<Rycerz>();
        }

        private static List<Rycerz> LosowanieBroniDlaRycerzy(List<Rycerz> rycerze, List<Bron> bronie)
        {
            Random rnd = new Random();
            foreach (var rycerz in rycerze)
            {
                rycerz.bron = bronie[rnd.Next(0, 5)];
            }
            return rycerze;
        }

        private static List<Bron> LosowanieCzestotliwosciLeczeniaBroniAll(List<Bron> bronie)
        {
            Random rnd = new Random();
            foreach (var bron in bronie)
            {
                if (bron.leczenie == true)
                {
                    bron.tura = rnd.Next(1, 4);
                }
            }
            return bronie;
        }

        private static void WypiszGraczy(List<Rycerz> walczacy)
        {
            int x = 1;
            foreach (var rycerz in walczacy)
            {
                Console.WriteLine($"Rycerz {x}: {rycerz.name} Zdrowie: {rycerz.zdrowie}  Siła: {rycerz.sila}");
                Console.WriteLine($"Broń: {rycerz.bron.GetName()} \nAtak: ({rycerz.bron.atak_min} - {rycerz.bron.atak_max})" +
                $"\nObrona: ({rycerz.bron.obrona_min} - {rycerz.bron.obrona_max}) \nLeczenie: {rycerz.bron.ilosc_zdrowia} \nCzęstotliwość leczenia: {rycerz.bron.tura} \n");
                x++;
            }
        }

        private static void WaitForKey()
        {
            Console.WriteLine("(Aby kontynuować naciśnij dowolny klawisz, aby zakończyć naciśnij Esc)");
            var input = Console.ReadKey();
            if (input.Key == ConsoleKey.Escape)
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        private static void WaitForKeyEnd()
        {
            Console.WriteLine("(Aby zakończyć naciśnij Esc)");
            var input = Console.ReadKey();
            if (input.Key == ConsoleKey.Escape)
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }
    }
}
