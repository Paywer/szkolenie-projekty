﻿using System;
using System.Collections.Generic;

public class Bron : IBron
{
	private string name;
	public int atak_max, atak_min, obrona_min, obrona_max, ilosc_zdrowia, tura;
	public Boolean leczenie;
	public int licznik;

	public Bron(string name, int atak_min, int atak_max, int obrona_min, int obrona_max, Boolean leczenie, int ilosc_zdrowia)
	{
		this.name = name;
		this.atak_max = atak_max;
		this.atak_min = atak_min;
		this.obrona_min = obrona_min;
		this.obrona_max = obrona_max;
		this.leczenie = leczenie;
		this.ilosc_zdrowia = ilosc_zdrowia;
		tura = 0;
		licznik = 0;

	}

	public void LosowanieCzestotliwosciLeczenia()
	{
		Random rnd = new Random();
		tura = rnd.Next(1, 4);
	}

	public string GetName()
	{
		return this.name;
	}
}
