using System;
using Xunit;

namespace GradeBook.Tests
{
    public class BookTests
    {
        [Fact]
        public void GetBookReturnsDifferentObjects()
        {
            //arrange
            var book = new InMemoryBook("");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.3);
            
            //act
            var result = book.GetStatistics();
            
            //assert
            Assert.Equal(85.6, result.Average, 1);
            Assert.Equal(90.5, result.High, 1);
            Assert.Equal(77.3, result.Low, 1);
            Assert.Equal('B', result.Letter);
        }

        [Fact]
        public void GradeValueTest()
        {
            //arrange
            var book = new InMemoryBook("Book 1");
            double grade1, grade2, grade3; 

            grade1 = 0;
            grade2 = -1;
            grade3 = 101; 

            //act
            try
            {
                book.AddGrade(grade1);
                book.AddGrade(grade2);
                book.AddGrade(grade3);
            }
            catch (Exception)
            {}
            //assert

            Assert.Equal(0, book.grades[0]);
            Assert.StrictEqual(1, book.grades.Count);
        }

    }
}
