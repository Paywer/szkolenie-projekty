using System;
using Xunit;

namespace GradeBook.Tests
{
    public delegate string WriteLogDelegate(string logMessage);

    public class TypeTests
    {
        [Fact]
        public void WriteLogDelegateCanPointToMethod()
        {
            WriteLogDelegate log;

            log = ReturnMessage; //log = new WriteLogDelegate(ReturnMessage);

            var result = log("Hello!!!");
            Assert.Equal("Hello!!!", result);
        }

        string ReturnMessage(string message)
        {
            return message;
        }

        [Fact]
        public void ValueTypesAlsoPassByValue()
        {
            //arrange
            var x = GetInt();
            SetInt( ref x);

            //act            

            //assert
            Assert.Equal(42, x);
        }

        private void SetInt(ref int x)
        {
            x = 42;
        }

        private int GetInt()
        {
            return 3;
        }


        [Fact]
        public void CSharpCanPassByValueRef()
        {
            //arrange
            var book1 = GetBook("Book 1");
            GetBookSetName1(ref book1, "New Name"); 

            //act            

            //assert
            Assert.Equal("New Name", book1.Name);
        }

        private void GetBookSetName1(ref InMemoryBook book, string name)  //out Book book... musi być zainicjowany
        {
            book = new InMemoryBook(name); // potrzebne przy parametrze out
        }

        [Fact]
        public void CSharpIsPassByValue()
        {
            //arrange
            var book1 = GetBook("Book 1");
            GetBookSetName(book1, "New Name"); 

            //act            

            //assert
            Assert.Equal("Book 1", book1.Name);
        }

        private void GetBookSetName(InMemoryBook book, string name)
        {
            book = new InMemoryBook(name);
        }
        /*
        [Fact]
        public void CanSetNameFromReference()
        {
            //arrange
            var book1 = GetBook("Book 1");
            SetName(book1, "New Name"); 

            //act
            

            //assert
            Assert.Equal("New Name", book1.Name);
        }

        private void SetName(Book book, string name)
        {
            book.Name = name;
        }
        */
        [Fact]
        public void GetBookReturnsDifferentObjects()
        {
            //arrange
            var book1 = GetBook("Book 1");
            var book2 = GetBook("Book 2");

            //act
            

            //assert
            Assert.Equal("Book 1", book1.Name);
            Assert.Equal("Book 2", book2.Name);
            Assert.NotSame(book1, book2);
        }

        [Fact]
        public void TwoVarsCanReferenceSameObject()
        {
            //arrange
            var book1 = GetBook("Book 1");
            var book2 = book1;

            //assert
            Assert.Same(book1, book2);
            Assert.True(Object.ReferenceEquals(book1, book2));
        }

        InMemoryBook GetBook(string name)
        {
            return new InMemoryBook(name);
        }
    }
}
