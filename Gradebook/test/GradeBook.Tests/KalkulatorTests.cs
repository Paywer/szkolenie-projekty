using System;
using Xunit;

namespace GradeBook.Tests
{
    public class KalkulatorTests
    {
        [Fact]
        public void Silnia_Test1()
        {
            //arrange
            // var K = new Kalkulator();

            //act
            var wynik = Kalkulator.Silnia(0);
            
            //assert
            Assert.Equal(1, wynik);
        }

        [Fact]
        public void Silnia_Test2()
        {
            //arrange
             //var K = new Kalkulator();

            //act
            var wynik = Kalkulator.Silnia(3);
            
            //assert
            Assert.Equal(6, wynik);
        }

        [Fact]
        public void Silnia_Test3()
        {
            //arrange
             //var K = new Kalkulator();

            //act
            //var x = -1;
            
            //var wynik = Kalkulator.Silnia(x);
            // var ex = Assert.Throws<Exception>(() => wynik);
        


            //assert
            //Assert.Throws<Exception>(new Exception("tr"));
            //Assert.Equal(-1, wynik);
            //Assert.Same($"Nieprawidłowa wartość. Silnia z: {x} nie istnieje.", ex.Message);
        }
    }
}
