using System;

namespace GradeBook
{
     public class Kalkulator
    {
        public static int Silnia(int x)
        {
            int sil = 1;

            if (x == 0)
                return 1;
            else if (x > 0)
            {
                while(x > 1)
                {
                    sil *= x;
                    x--;
                }
                return sil;
            } 
            else
                {
                throw new ArgumentException($"Nieprawidłowa wartość. Silnia z: {x} nie istnieje.");
                }
        }

        public static int Silnia3(int x)
        {
            if(x == 1 || x == 0)
                return 1;
            else if (x < 0)
                throw new Exception();

            return x * Silnia3( --x );
        }
         


    }
}