﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GradeBook
{
    class Program
    {
    
        static void Main(string[] args)
        {
            // zczytywanie ścieżki programu
            //var dupa = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            IBook book = new DiskBook("Książka Scott'a");
            book.GradeAdded += OnGradeAdded;

            EnterGrades(book);

            var stats = book.GetStatistics();

            //Console.WriteLine(InMemoryBook.CATEGORY);
            Console.WriteLine($"Dla książki o tytule: {book.Name}");
            Console.WriteLine($"Średnia ocen wynosi {stats.Average:N1}");
            Console.WriteLine($"Najniższa ocena wynosi {stats.Low:N1}");
            Console.WriteLine($"Najwyższa ocena wynosi {stats.High:N1}");
            Console.WriteLine($"Ocena literowa to {stats.Letter}");

            /* var grades = new List<double>() {12.7, 10.3, 6.11, 4.1};  // List<double> grades = ...
             grades.Add(56.1);

             */
            // silnia    

            int l1 = 1;

            try
            {

                var wartosc = Kalkulator.Silnia(l1);
                Console.WriteLine($"Silnia z liczby {l1} wynosi {wartosc}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void EnterGrades(IBook book)
        {
            Console.WriteLine("Podaj ocenę do wpisania lub wciśnij 'q' aby zakończyć.");
            var input = Console.ReadLine();

            while (input != "q" && input != "Q")
            {
                try
                {
                    
                    
                    book.AddGrade(double.Parse(input));
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                    // throw;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    // kod który ma się wykonać mimo wyjątków
                }

                Console.WriteLine("Podaj kolejną ocenę do wpisania lub wciśnij 'q' aby zakończyć.");
                input = Console.ReadLine();
            }
            /*
            while(true)
            {
                Console.WriteLine("Podaj ocenę do wpisania lub wciśnij 'q' aby zakończyć.");
                var input = Console.ReadLine();

                if (input == "q")
                { 
                    break;
                }

                var grade = double.Parse(input);
                book.AddGrade(grade);
            }
            */

            /*
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.5);
            */

        }

        static void OnGradeAdded(object sender, EventArgs e)
        {
            Console.WriteLine("Ocena została dodana.");
        }

        
            /* kod z Main()
            var x = 34.1;
            var y = 10.3;
            var wynik = x + y;
            Console.WriteLine($"{wynik:N3}");
            var liczby = new [] {12.7, 10.3, 6.11, 4.1}; // double[] liczby =new double[3];
            liczby[0] = 12.7;
            liczby[1] = 10.3;
            liczby[2] = 6.11;

            var wynik2 = liczby[0];
            wynik2 += liczby[1];
            wynik2 = wynik2 + liczby[2];
            */

            /* alternatywna funkcja ForEach
            var wynik3 = 0.0;
            oceny.ForEach(item => {
                wynik3 += item;
                Console.WriteLine(wynik3);
            }); */
    }
}
