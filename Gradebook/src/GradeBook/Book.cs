using System;
using System.Collections.Generic;
using System.IO;

namespace GradeBook
{
    public delegate void GradeAddedDelegate(object sender, EventArgs args);

    public class NamedObject
    {
        public NamedObject(string name)
        {
            Name = name;
        }

        public string Name
        {
            get;
            set;
        }
    }

    public interface IBook
    {
        void AddGrade(double grade);
        Statistics GetStatistics();
        string Name
        {
            get;
        }
        event GradeAddedDelegate GradeAdded;
    }

    public abstract class Book : NamedObject, IBook
    {
        public Book(string name) : base(name)
        {
       }

        public abstract event GradeAddedDelegate GradeAdded; //wcześniej public virtual ... czemu zmiana??

        public abstract void AddGrade(double grade);

        public abstract Statistics GetStatistics();

    }

    public class DiskBook : Book
    {
        public DiskBook(string name) : base(name)
        {
        }

        public override event GradeAddedDelegate GradeAdded;

        public override void AddGrade(double grade)
        {
            


            using (var writer = File.AppendText($"{Name}.txt"))  //wzór do używania i zamykania obiektów
            {
                
                if (grade <= 100 && grade >= 0)
                {
                    writer.WriteLine(grade);
                    if (GradeAdded != null)
                    {
                        GradeAdded(this, new EventArgs());
                    }

                }
                else
                {
                    throw new ArgumentException($"Invalid {nameof(grade)}");
                }

            }
            //writer.Close(); writer.Dispose*();
        }

        public override Statistics GetStatistics()
        {
            var result = new Statistics();
            using (var reader = File.OpenText($"{Name}.txt"))
            {
                var line = reader.ReadLine();
                while (line != null)
                {
                    var number = double.Parse(line);
                    result.Add(number);
                    line = reader.ReadLine();
                }
            }
            return result;
        }
    }

    public class InMemoryBook : Book
    {
        public List<double> grades
        {
            get;
            private set;
        }

        //readonly string category = "Science";  //const - stała, nawet w konstr. nie można jej zmienić
        public const string CATEGORY = "Science";

        public InMemoryBook(string name) : base(name)
        {

            //grades = new List<double>();
            //Name = name;  
        }

        public void AddGrade(char letter)
        {
            switch (letter)
            {
                case 'A':
                    AddGrade(90);
                    break;

                case 'B':
                    AddGrade(80);
                    break;

                case 'C':
                    AddGrade(70);
                    break;

                case 'D':
                    AddGrade(60);
                    break;

                case 'E':
                    AddGrade(50);
                    break;

                default:
                    AddGrade(0);
                    break;
            }
        }


        public override void AddGrade(double grade)
        {
            if (grade <= 100 && grade >= 0)
            {
                grades.Add(grade);

                if (GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }

            }
            else
            {
                throw new ArgumentException($"Invalid {nameof(grade)}");
            }
        }

        public override event GradeAddedDelegate GradeAdded;

        public override Statistics GetStatistics()
        {
            var result = new Statistics();
            //result.Average = 0.0;
            //result.High = double.MinValue;
            //result.Low = double.MaxValue;

            for (var index = 0; index < grades.Count; index++)
            {
                result.Add(grades[index]);
                //result.High = Math.Max(grades[index], result.High);
                //result.Low = Math.Min(grades[index], result.Low);
                //result.Average += grades[index];
            }

            /*
            foreach(var grade in grades)
            {
                result.High = Math.Max(grade, result.High);
                result.Low = Math.Min(grade, result.Low);
                result.Average += grade;
                
            }
            */

            //result.Average /= grades.Count;

            /* switch(result.Average)
             {
                 case var d when d >= 90.0:
                     result.Letter = 'A';
                     break;

                 case var d when d >= 80.0:
                     result.Letter = 'B';
                     break;

                 case var d when d >= 70.0:
                     result.Letter = 'C';
                     break;

                 case var d when d >= 60.0:
                     result.Letter = 'D';
                     break;

                 case var d when d >= 50.0:
                     result.Letter = 'E';
                     break;

                 default:
                     result.Letter = 'F';
                     break;       
             } */
            return result;
        }


    }
}