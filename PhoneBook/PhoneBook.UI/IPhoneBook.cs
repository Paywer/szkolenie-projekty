﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.UI
{
    public interface IPhoneBook
    {
        void ShowAll();
        void Search();
        void Edit(string bookpath);
        void Delete(string bookpath, string idpath);
        void Add(string bookpath, string idpath);
    }
}
