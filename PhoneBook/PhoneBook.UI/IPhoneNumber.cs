﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.UI
{
    public interface IPhoneNumber
    {
        void SetTyp(string value);
        void SetKierunkowy(string value);
        void SetNumer(string value);
    }
}
