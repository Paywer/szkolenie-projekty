﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.UI
{
    public interface IPerson
    {
        void SetLname(string lname);
        void SetFname(string fname);
        void SetMarital(string marital);
    }
}
