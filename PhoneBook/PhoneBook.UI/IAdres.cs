﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.UI
{
    public interface IAdres
    {
        void SetPostCode(string value);
        void SetCity(string value);
        void SetStreet(string value);
        void SetHomeNumber(string value);
        void SetFlatNumber(string value);
    }
}
