﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace PhoneBookNamespace.BL
{
    public class CsvReader
    {
		private string csvFilePath;

		public CsvReader(string csvFilePath)
		{
			this.csvFilePath = csvFilePath;
		}

		public List<Person> ReadAll()
		{
			List<Person> Lista = new List<Person>();
			using (StreamReader sr = new StreamReader(csvFilePath))
			{
				//sr.ReadLine();

				string csvLine;
				while ((csvLine = sr.ReadLine()) != null)
				{
					Lista.Add(ReadFromCsvLine(csvLine));
					
				}
			}
			//Lista = Lista.OrderBy(person => person.Adres.City).ThenBy(person => person.LName).ThenBy(person => person.FName).ToList();
			return Lista;
		}

		public Person ReadFromCsvLine(string csvLine)
		{
			string[] parts = csvLine.Split(',');
			string Id;
			string LName;
			string FName;
			string Marital;
			
			List<PhoneNumber> PhoneList = new List<PhoneNumber>{};
			
			Id = parts[0];
			LName = parts[1];
			FName = parts[2];
			Marital = parts[3];
			Adres Adres = new Adres(parts[4], parts[5], parts[6], parts[7], parts[8]);

			int i = 11;
			while (i < parts.Length)
			{
				PhoneList.Add(new PhoneNumber(parts[i - 2], parts[i - 1], parts[i]));
				i += 3;
			}

			return new Person(Id, LName, FName, Marital, Adres, PhoneList);
		}
		//-------------------------------------------READER 2---------------------------------------------------------------
		public List<string> ReadFreeId()
		{
			List<string> freeId;
			using (StreamReader sr = new StreamReader(csvFilePath))
			{
				freeId = sr.ReadLine().Split(',').ToList();
			}
			return freeId;
		}

	}
}
