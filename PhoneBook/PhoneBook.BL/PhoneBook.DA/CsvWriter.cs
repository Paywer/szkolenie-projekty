﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PhoneBookNamespace.BL
{
    class CsvWriter
    {
        private string csvFilePath;

        public CsvWriter(string csvFilePath)
        {
            this.csvFilePath = csvFilePath;
        }

		public void WriteAll(List<Person> Lista)
		{
			if (File.Exists(csvFilePath))
			{
				File.Delete(csvFilePath);
			}

			Lista = Lista.OrderBy(person => person.Adres.City).ThenBy(person => person.LName).ThenBy(person => person.FName).ToList();
			using (StreamWriter sr = new StreamWriter(csvFilePath))
			{
				foreach (Person person in Lista)
				{
					sr.WriteLine(ConvertPersonToCsv(person));
				}
			}
		}

		public string ConvertPersonToCsv(Person Person)
		{
			string LINE ="";
			string Id = Person.Id;
			string LName = Person.LName;
			string FName = Person.FName;
			string Marital = Person.Marital;
			string Adres1 = Person.Adres.PostCode;
			string Adres2 = Person.Adres.City;
			string Adres3 = Person.Adres.Street;
			string Adres4 = Person.Adres.HomeNumber;
			string Adres5 = Person.Adres.FlatNumber;

			LINE = LINE + Id;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + LName;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + FName;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + Marital;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + Adres1;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + Adres2;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + Adres3;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + Adres4;
			LINE = LINE.Trim();
			LINE = LINE + ",";
			LINE = LINE.Trim();
			LINE = LINE + Adres5;
			LINE = LINE.Trim();


			foreach (PhoneNumber phone in Person.PhoneList)
			{
				LINE = LINE + ",";
				LINE = LINE.Trim();
				LINE = LINE + phone.Typ;
				LINE = LINE.Trim();
				LINE = LINE + ",";
				LINE = LINE.Trim();
				LINE = LINE + phone.Kierunkowy;
				LINE = LINE + ",";
				LINE = LINE.Trim();
				LINE = LINE + phone.Numer;
				LINE = LINE.Trim();
				
			}
			return LINE;
		}
		//-------------------------------------------WRITER 2---------------------------------------------------------------
		public void WriteFreeId(List<string> freeId)
		{
			if (File.Exists(csvFilePath))
			{
				File.Delete(csvFilePath);
			}

			using (StreamWriter sr = new StreamWriter(csvFilePath))
			{
					sr.WriteLine(ConvertFreeIdToCsv(freeId));
			}
		}

		public string ConvertFreeIdToCsv(List<string> freeId)
		{
			string LINE = "";
			LINE = LINE + freeId[0];
			LINE = LINE.Trim();

			if (freeId.Count > 1)
			{
				for (int i = 1; i < freeId.Count; i++)
				{
					LINE = LINE + ",";
					LINE = LINE.Trim();
					LINE = LINE + freeId[i];
					LINE = LINE.Trim();
				}
			}
			return LINE;
		}
	}
}
