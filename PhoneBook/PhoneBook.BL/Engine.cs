﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PhoneBookNamespace.BL
{
    class Engine
    {
        public static void RunPhoneBook()
        {
			PhoneBok phoneBook = new PhoneBok();
			var bookPath = @"E:\Programowanie\Projekty\szkolenie-projekty\PhoneBook\PhoneBook.csv";
			var idPath = @"E:\Programowanie\Projekty\szkolenie-projekty\PhoneBook\FreeID.csv";
			CsvReader reader = new CsvReader(bookPath);
			phoneBook.PersonList = new List<Person>();
			if (File.Exists(bookPath))
			{
				phoneBook.PersonList = reader.ReadAll();
			}

			while (true)
			{
				Display.MainMenu();
				
				switch (WaitForKey('5'))
				{
					case '1':
						phoneBook.ShowAll();
						break;
					case '2':
						phoneBook.Search();
						break;
					case '3':
						phoneBook.Add(bookPath, idPath);
						phoneBook.PersonList = phoneBook.PersonList.OrderBy(person => person.Adres.City).ThenBy(person => person.LName).ThenBy(person => person.FName).ToList();
						break;
					case '4':
						phoneBook.Edit(bookPath);
						break;
					case '5':
						phoneBook.Delete(bookPath, idPath);
						break;
					default:
						throw new Exception("\n !!! Nieprawidłowa wartość. !!!");
				}
			}
		}
		//----------------------------------------METODY POMOCNICZE------------------------------------------------------------
		public static char WaitForKey(char number)
		{
			var input = Console.ReadKey();
			if (input.Key == ConsoleKey.Escape)
			{
				System.Diagnostics.Process.GetCurrentProcess().Kill();
			}

			char output = input.KeyChar;
			if (input.KeyChar > number || input.KeyChar < '1')
			{
				Console.WriteLine("\n !! Nieprawidłowa wartość. Wybierz ponownie. !!");
				output = WaitForKey(number);
			}
			return output;
		}

	}
}
