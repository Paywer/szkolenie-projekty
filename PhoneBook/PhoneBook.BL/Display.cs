﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace PhoneBookNamespace.BL
{
    public static class Display
    {
        public static void MainMenu()
        {
            Console.Clear();

            Console.Title = "KSIĄŻKA TELEFONICZNA";
            Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine(" 1. Wyświetl całą książkę\n" +
                " 2. Wyszukaj pozycję\n" +
                " 3. Dodaj pozycję\n" +
                " 4. Edytuj pozycję\n" +
                " 5. Usuń pozycję\n");
            Console.WriteLine("\n   (Wciśnij ESC aby zamknąć program)");
        }

        public static void Menu1(List<Person> lista)
        {
            bool y = true;

            Console.Clear();
            Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("         |Wszystkie numery|");
            Console.WriteLine("----------------------------------------------------");

            foreach (Person person in lista)
            {
                Console.Write($"\n {person.Id}| {person.Adres.City} | {person.LName} {person.FName} | ");
                foreach (PhoneNumber numer in person.PhoneList)
                {
                    Console.Write($"{numer.Kierunkowy} {numer.Numer}, ");
                }

                if (person.PhoneList.Count == 0)
                    Console.Write("(brak numeru)");
            }

            y = Details(lista);
            if (y == true)
                Menu1(lista);
        }

        public static void Menu2(List<Person> lista)
        {
            void MainMenu2()
            {
                Console.Clear();
                Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("         |Wyszukiwanie pozycji|");
                Console.WriteLine("----------------------------------------------------");
            }

            bool x = false;
            string p1 = "", p2 = "", p3 = "", p4 = "", p5 = "", p6 = "", p7 = "", p7a = "", p7b = "", p8 = "", p9 = "";
            do
            {
                MainMenu2();
                Console.WriteLine(" Dodaj kryteria wyszukiwania");
                Console.WriteLine("- - - - - - - - - - - - - - - -\n");
                Console.WriteLine($" 1. Nazwisko: {p1}\n" +
                $" 2. Imię: {p2}\n" +
                $" 3. Stan cywilny: {p3}\n" +
                $" 4. Kod pocztowy(format xx-xxx): {p4}\n" +
                $" 5. Miasto: {p5}\n" +
                $" 6. Ulica: {p6}\n" +
                $" 7. Numer budynku/lokalu: {p7}\n" +
                $" 8. Numer telefonu: {p8}{p9}\n" +
                $"\n 9. WYSZUKAJ\n");
                Console.WriteLine("\n   (Wciśnij ESC aby zamknąć program)");

                switch (Engine.WaitForKey('9'))
                {
                    case '1':
                        MainMenu2();
                        Console.WriteLine(" Podaj Nazwisko(bez imienia): ");
                        p1 = Console.ReadLine();
                        break;
                    case '2':
                        MainMenu2();
                        Console.WriteLine(" Podaj Imię: ");
                        p2 = Console.ReadLine();
                        break;
                    case '3':
                        MainMenu2();
                        Console.WriteLine(" Podaj Stan Cywilny: ");
                        p3 = Console.ReadLine();
                        break;
                    case '4':
                        MainMenu2();
                        Console.WriteLine(" Podaj Kod Pocztowy(format xx-xxx): ");
                        p4 = Console.ReadLine();
                        break;
                    case '5':
                        MainMenu2();
                        Console.WriteLine(" Podaj Miasto: ");
                        p5 = Console.ReadLine();
                        break;
                    case '6':
                        MainMenu2();
                        Console.WriteLine(" Podaj Nazwę Ulicy: ");
                        p6 = Console.ReadLine();
                        break;
                    case '7':
                        MainMenu2();
                        Console.WriteLine(" Podaj Numer Budynku: ");
                        p7a = Console.ReadLine();
                        MainMenu2();
                        Console.WriteLine(" Podaj Numer Lokalu:     (wciśnij Enter żeby pominąć)");
                        p7b = Console.ReadLine();
                        p7 = p7a;

                        if (p7b != "")
                        {
                            p7 = p7 + "/" + p7b;
                            p7 = p7.Trim();
                        }
                        break;
                    case '8':
                        MainMenu2();
                        Console.WriteLine("     Podaj Numer Kierunkowy:     (wciśnij Enter żeby pominąć)\n");
                        p8 = Console.ReadLine();
                        MainMenu2();
                        Console.WriteLine("     Podaj Numer telefonu: ");
                        p9 = Console.ReadLine();
                        break;
                    case '9':
                        x = true;
                        break;
                }
            }
            while (x == false);

            List<Person> lista2 = new List<Person>(lista);
            if (p1 != "")            
                lista2 = lista2.Where(x => x.LName == p1).ToList();

            if (p2 != "")
                lista2 = lista2.Where(x => x.FName == p2).ToList();

            if (p3 != "")
                lista2 = lista2.Where(x => x.Marital == p3).ToList();

            if (p4 != "")
                lista2 = lista2.Where(x => x.Adres.PostCode == p4).ToList();

            if (p5 != "")
                lista2 = lista2.Where(x => x.Adres.City == p5).ToList();

            if (p6 != "")
                lista2 = lista2.Where(x => x.Adres.Street == p6).ToList();

            if (p7a != "")
                lista2 = lista2.Where(x => x.Adres.HomeNumber == p7a).ToList();

            if (p7b != "")
                lista2 = lista2.Where(x => x.Adres.FlatNumber == p7b).ToList();

            if (p8 != "")
                lista2 = lista2.Where(x => x.PhoneList.Exists(y => y.Kierunkowy == p8)).ToList();

            if (p9 != "")
                lista2 = lista2.Where(x => x.PhoneList.Exists(y => y.Numer == p9)).ToList();

            if (lista2.Count != 0)
            {
                bool y = true;
                do
                {
                    MainMenu2();
                    Console.WriteLine($"Znaleziono {lista2.Count} pozycji:\n");
                    foreach (Person person in lista2)
                    {
                        Console.Write($"\n {person.Id}| {person.Adres.City} | {person.LName} {person.FName} | ");
                        foreach (PhoneNumber numer in person.PhoneList)
                        {
                            Console.Write($"{numer.Kierunkowy} {numer.Numer}, ");
                        }

                        if (person.PhoneList.Count == 0)
                            Console.Write("(brak numeru)");
                    }

                    y = Details(lista2);
                }
                while (y == true);
            }
            else
            {
                MainMenu2();
                Console.WriteLine($"  Nie znaleziono żadnej pozycji!\n");
                Console.WriteLine("\n (Wciśnij dowolny klawisz aby kontynuować)");
                Console.ReadKey();
            }
        }

        public static void Menu3(List<Person> lista, string bookpath, string idpath)
        {
            void MainMenu3()
            {
                Console.Clear();
                Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("         |Dodawanie pozycji|");
                Console.WriteLine("----------------------------------------------------");
            }

            MainMenu3();
            CsvReader reader2 = new CsvReader(idpath);

            List<string> freeId = new List<string>();
            if (File.Exists(idpath))
            {
                freeId = reader2.ReadFreeId();
            }
            else
                freeId.Add("1");

            Person person;
            if (freeId.Count == 1)
                person = new Person(freeId[0]);
            else
                person = new Person(freeId[1]);

            Console.WriteLine("     Podaj Nazwisko(bez imienia):     (wciśnij Enter żeby pominąć)");
            string line = Console.ReadLine();

            if (line != "")
            {
                person.SetLname(line);
            }

            MainMenu3();
            Console.WriteLine("     Podaj Imię:     (wciśnij Enter żeby pominąć)");
            line = Console.ReadLine();

            if (line != "")
                person.SetFname(line);

            MainMenu3();
            Console.WriteLine("     Podaj Stan Cywilny:     (wciśnij Enter żeby pominąć)");
            line = Console.ReadLine();

            if (line != "")
                person.SetMarital(line);

            MainMenu3();
            Console.WriteLine(" 1. Dodaj Adres\n" +
                " 2. Pomiń\n");
            Console.WriteLine("\n   (Wciśnij ESC aby anulować i zamknąć program)");

            if (Engine.WaitForKey('2') == '1')
            {
                MainMenu3();
                Console.WriteLine("     Podaj Kod Pocztowy(format xx-xxx):     (wciśnij Enter żeby pominąć)");
                line = Console.ReadLine();

                if (line != "")
                    person.Adres.SetPostCode(line);

                MainMenu3();
                Console.WriteLine("     Podaj Miasto:     (wciśnij Enter żeby pominąć)");
                line = Console.ReadLine();

                if (line != "")
                    person.Adres.SetCity(line);

                MainMenu3();
                Console.WriteLine("     Podaj Nazwę Ulicy:     (wciśnij Enter żeby pominąć)");
                line = Console.ReadLine();

                if (line != "")
                    person.Adres.SetStreet(line);

                MainMenu3();
                Console.WriteLine("     Podaj Numer Budynku:     (wciśnij Enter żeby pominąć)");
                line = Console.ReadLine();

                if (line != "")
                    person.Adres.SetHomeNumber(line);

                MainMenu3();
                Console.WriteLine("     Podaj Numer Lokalu:     (wciśnij Enter żeby pominąć)");
                line = Console.ReadLine();

                if (line != "")
                    person.Adres.SetFlatNumber(line);
            }

            MainMenu3();
            Console.WriteLine(" 1. Dodaj Numer telefonu\n" +
                " 2. Pomiń\n");
            Console.WriteLine("\n   (Wciśnij ESC aby anulować i zamknąć program)");

            while (Engine.WaitForKey('2') == '1')
            {
                PhoneNumber numer = new PhoneNumber();
                MainMenu3();
                Console.WriteLine("     Wybierz Typ Numeru:     (wciśnij Enter żeby pominąć)");
                Console.WriteLine(" 1. Komórkowy\n" +
                " 2. Stacjonarny\n" +
                " 3. Pomiń\n");
                Console.WriteLine("\n   (Wciśnij ESC aby anulować i zamknąć program)");
                

                switch (Engine.WaitForKey('3'))
                {
                    case '1':
                        numer.SetTyp("Komórkowy");
                        break;
                    case '2':
                        numer.SetTyp("Stacjonarny");
                        MainMenu3();
                        Console.WriteLine("     Podaj Numer Kierunkowy: \n");
                        line = Console.ReadLine();
                        while (line == "")
                        {
                            Console.WriteLine("\n   ! Pole Numer Kierunkowy nie może być puste. Proszę podać Numer Kierunkowy !  ");
                            line = Console.ReadLine();
                        }
                        numer.SetKierunkowy(line);
                        break;
                    case '3':
                        break;
                    default:
                        throw new Exception("\n !!! Nieprawidłowa wartość. !!!");
                }

                MainMenu3();
                Console.WriteLine("     Podaj Numer telefonu:     (wciśnij Enter żeby pominąć)");
                line = Console.ReadLine();

                if (line != "")
                {
                    numer.SetNumer(line);
                    person.PhoneList.Add(numer);
                    MainMenu3();
                    Console.WriteLine(" [Nie dodano numeru telefonu].\n ");
                }
                else
                {
                    MainMenu3();
                    Console.WriteLine(" [Numer telefonu został dodany].\n ");
                }

                Console.WriteLine("\n 1. Dodaj kolejny numer telefonu\n" +
                    " 2. Zapisz pozycję");
            }

            lista.Add(person);
            CsvWriter writer = new CsvWriter(bookpath);
            CsvWriter writer2 = new CsvWriter(idpath);

            if (freeId.Count == 1)
            {
                int id2 = Convert.ToInt32(freeId[0]);
                id2++;
                freeId[0] = id2.ToString();
            }
            else
                freeId.RemoveAt(1);

            writer.WriteAll(lista);
            writer2.WriteFreeId(freeId);
         
            MainMenu3();
            Console.WriteLine(" [Pozycja została dodana]\n");
            Console.WriteLine("\n (Wciśnij dowolny klawisz aby kontynuować)");
            Console.ReadKey();

        }

        public static void Menu4(List<Person> lista, string bookpath)
        {
            void MainMenu4()
            {
                Console.Clear();
                Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("         |Edytowanie pozycji|");
                Console.WriteLine("----------------------------------------------------");
            }

            MainMenu4();

            Console.WriteLine(" Podaj ID pozycji do edytowania: ");
            bool x = true;
            do
            {
                string line = Console.ReadLine();
                List<Person> person = lista.Where(x => x.Id == line).ToList();
                CsvWriter writer = new CsvWriter(bookpath);

                if (person.Count == 1)
                {
                    MainMenu4();
                    Console.Write($"\n {person[0].Id}| {person[0].Adres.City} | {person[0].LName} {person[0].FName} | ");
                    foreach (PhoneNumber numer in person[0].PhoneList)
                    {
                        Console.Write($"{numer.Kierunkowy} {numer.Numer}, ");
                    }

                    if (person[0].PhoneList.Count == 0)
                        Console.Write("(brak numeru)");

                    Console.WriteLine("\n\n Który element chcesz edytować?\n");
                    Console.WriteLine(" 1. Nazwisko i Imię\n" +                        
                        " 2. Adres\n" +
                        " 3. Numer telefonu\n" +
                        "\n 4. Powrót do Menu\n");
                    Console.WriteLine("\n   (Wciśnij ESC aby anulować i zamknąć program)");

                    switch (Engine.WaitForKey('4'))
                    {
                        case '1':
                            MainMenu4();
                            Console.WriteLine("     Podaj nowe Nazwisko(bez imienia) lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();
                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].SetLname("");
                            else if (line != "")                   
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].SetLname(line);                                
                            

                            MainMenu4();
                            Console.WriteLine("     Podaj nowe Imię lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();

                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].SetFname("");
                            else if (line != "")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].SetFname(line);
                            
                            writer.WriteAll(lista);
                            MainMenu4();
                            Console.WriteLine(" [Pozycja została zmieniona]\n");
                            Console.WriteLine("\n (Wciśnij dowolny klawisz aby kontynuować)");
                            Console.ReadKey();
                            break;
                        case '2':
                            MainMenu4();
                            Console.WriteLine("     Podaj nowy Kod Pocztowy(format xx-xxx) lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();
                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetPostCode("");
                            else if (line != "")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetPostCode(line);


                            MainMenu4();
                            Console.WriteLine("     Podaj nowe Miasto lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();

                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetCity("");
                            else if (line != "")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetCity(line);

                            MainMenu4();
                            Console.WriteLine("     Podaj nową Nazwę Ulicy lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();

                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetStreet("");
                            else if (line != "")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetStreet(line);
                            MainMenu4();
                            Console.WriteLine("     Podaj nowy Numer Budynku lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();

                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetHomeNumber("");
                            else if (line != "")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetHomeNumber(line);
                            MainMenu4();
                            Console.WriteLine("     Podaj nowy Numer Lokalu lub '0' aby usunąć pole:     (wciśnij Enter żeby pominąć)");
                            line = Console.ReadLine();

                            if (line == "0")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetFlatNumber("");
                            else if (line != "")
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].Adres.SetFlatNumber(line);

                            writer.WriteAll(lista);
                            MainMenu4();
                            Console.WriteLine(" [Pozycja została zmieniona]\n");
                            Console.WriteLine("\n (Wciśnij dowolny klawisz aby kontynuować)");
                            Console.ReadKey();
                            break;
                        case '3':
                            MainMenu4();
                            Console.WriteLine(" Który numer chcesz edytować?\n");
                            int i = 1;
                            foreach (PhoneNumber numer in person[0].PhoneList)
                            {
                                Console.WriteLine($" {i}. {numer.Kierunkowy} {numer.Numer}");
                                i++;
                            }

                            if (person[0].PhoneList.Count == 0)
                                Console.WriteLine("(brak numeru)\n");

                            Console.WriteLine($"\n {i}. Dodaj nowy numer");
                            
                            if (person[0].PhoneList.Count != 0)
                                Console.WriteLine($" {i+1}. Usuń numer\n");
                            
                            char option = Engine.WaitForKey(Convert.ToChar((i + 1 + '0')));

                            if (option == Convert.ToChar((i + '0')))
                            {
                                PhoneNumber numer = new PhoneNumber();
                                MainMenu4();
                                Console.WriteLine(" Wybierz Typ Numeru:     (wciśnij Enter żeby pominąć)\n");
                                Console.WriteLine(" 1. Komórkowy\n" +
                                " 2. Stacjonarny\n" +
                                " 3. Pomiń\n");
                                Console.WriteLine("\n   (Wciśnij ESC aby anulować i zamknąć program)");


                                switch (Engine.WaitForKey('3'))
                                {
                                    case '1':
                                        numer.SetTyp("Komórkowy");
                                        break;
                                    case '2':
                                        numer.SetTyp("Stacjonarny");
                                        MainMenu4();
                                        Console.WriteLine("     Podaj Numer Kierunkowy: \n");
                                        line = Console.ReadLine();
                                        while (line == "")
                                        {
                                            Console.WriteLine("\n   ! Pole Numer Kierunkowy nie może być puste. Proszę podać Numer Kierunkowy !  ");
                                            line = Console.ReadLine();
                                        }
                                        numer.SetKierunkowy(line);
                                        break;
                                    case '3':
                                        break;
                                    default:
                                        throw new Exception("\n !!! Nieprawidłowa wartość. !!!");
                                }

                                MainMenu4();
                                Console.WriteLine("     Podaj Numer telefonu:     (wciśnij Enter żeby pominąć)");
                                line = Console.ReadLine();

                                if (line != "")
                                {
                                    numer.SetNumer(line);
                                    lista[lista.FindIndex(x => x.Id == person[0].Id)].PhoneList.Add(numer);
                                    MainMenu4();
                                    Console.WriteLine(" [Numer telefonu został dodany].");
                                }
                                else
                                {
                                    MainMenu4();
                                    Console.WriteLine(" [Nie dodano numeru telefonu].");
                                }
                            }
                            else if (option == Convert.ToChar(i + 1 + '0'))
                            {
                                MainMenu4();
                                Console.WriteLine(" Który numer chcesz usunąć?\n");
                                int j = 1;
                                foreach (PhoneNumber numer in person[0].PhoneList)
                                {
                                    Console.WriteLine($" {j}. {numer.Kierunkowy} {numer.Numer}");
                                    j++;
                                }
                                var key2 = Console.ReadKey();
                                lista[lista.FindIndex(x => x.Id == person[0].Id)].PhoneList.RemoveAt(Convert.ToInt32(key2.KeyChar - '0') - 1);
                                MainMenu4();
                                Console.WriteLine(" [Numer telefonu został usunięty].");
                            }
                            else
                            {
                                PhoneNumber numer = new PhoneNumber();
                                MainMenu4();
                                Console.WriteLine(" Stary numer zostanie usunięty !!\n");
                                Console.WriteLine(" Wybierz typ nowego Numeru:     (wciśnij Enter żeby pominąć)\n");
                                Console.WriteLine(" 1. Komórkowy\n" +
                                " 2. Stacjonarny\n" +
                                " 3. Pomiń\n");
                                Console.WriteLine("\n   (Wciśnij ESC aby anulować i zamknąć program)");


                                switch (Engine.WaitForKey('3'))
                                {
                                    case '1':
                                        numer.SetTyp("Komórkowy");
                                        break;
                                    case '2':
                                        numer.SetTyp("Stacjonarny");
                                        MainMenu4();
                                        Console.WriteLine("     Podaj Numer Kierunkowy: \n");
                                        line = Console.ReadLine();
                                        while (line == "")
                                        {
                                            Console.WriteLine("\n   ! Pole Numer Kierunkowy nie może być puste. Proszę podać Numer Kierunkowy !  ");
                                            line = Console.ReadLine();
                                        }
                                        numer.SetKierunkowy(line);
                                        break;
                                    case '3':
                                        break;
                                    default:
                                        throw new Exception("\n !!! Nieprawidłowa wartość. !!!");
                                }

                                MainMenu4();
                                Console.WriteLine("     Podaj Numer telefonu:     (wciśnij Enter żeby pominąć)");
                                line = Console.ReadLine();

                                if (line != "")
                                {
                                    numer.SetNumer(line);
                                    lista[lista.FindIndex(x => x.Id == person[0].Id)].PhoneList.RemoveAt(Convert.ToInt32(option - '0') - 1);
                                    lista[lista.FindIndex(x => x.Id == person[0].Id)].PhoneList.Add(numer);
                                    MainMenu4();
                                    Console.WriteLine(" [Numer telefonu został zmieniony].");
                                }
                                else
                                {
                                    MainMenu4();
                                    Console.WriteLine(" [Edycja numeru nieudana].");
                                }
                            }

                            writer.WriteAll(lista);
                            Console.WriteLine("\n (Wciśnij dowolny klawisz aby kontynuować)");
                            Console.ReadKey();
                            break;
                        case '4':
                            break;
                    }                                   
                    x = true;
                }
                else
                {
                    Console.WriteLine("\n !! Nieprawidłowa wartość. !! Podaj prawdiłowe ID: ");
                    x = false;
                }
            }
            while (x == false);

        }

        public static void Menu5(List<Person> lista, string bookpath, string idpath)
        {
            void MainMenu5()
            {
                Console.Clear();
                Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("         |Usuwanie pozycji|");
                Console.WriteLine("----------------------------------------------------");
            }

            MainMenu5();
            CsvReader reader2 = new CsvReader(idpath);

            List<string> freeId = new List<string>();
            if (File.Exists(idpath))
            {
                freeId = reader2.ReadFreeId();
            }
            else
                freeId.Add("1");

            Console.WriteLine(" Podaj ID pozycji do usunięcia: ");
            bool x = true;
            do
            {
                string line = Console.ReadLine();
                List<Person> person = lista.Where(x => x.Id == line).ToList();

                if (person.Count == 1)
                {
                    MainMenu5();
                    Console.Write($"\n {person[0].Id}| {person[0].Adres.City} | {person[0].LName} {person[0].FName} | ");
                    foreach (PhoneNumber numer in person[0].PhoneList)
                    {
                        Console.Write($"{numer.Kierunkowy} {numer.Numer}, ");
                    }

                    if (person[0].PhoneList.Count == 0)
                        Console.Write("(brak numeru)");

                    Console.WriteLine("\n\n Czy na pewno chcesz usunąć wyświetloną pozycję?\n");
                    Console.WriteLine(" 1. Usuń\n" +
                        " 2. Anuluj");

                    if (Engine.WaitForKey('2') == '1')
                    {
                        lista.RemoveAt(lista.FindIndex(x => x.Id == person[0].Id));
                        freeId.Add(person[0].Id);
                        CsvWriter writer = new CsvWriter(bookpath);
                        CsvWriter writer2 = new CsvWriter(idpath);
                        writer.WriteAll(lista);
                        writer2.WriteFreeId(freeId);

                        MainMenu5();
                        Console.WriteLine(" [Pozycja została usunięta]\n");
                        Console.WriteLine("\n (Wciśnij dowolny klawisz aby kontynuować)");
                        Console.ReadKey();
                    }
                    x = true;
                }
                else
                {
                    Console.WriteLine("\n !! Nieprawidłowa wartość. !! Podaj prawdiłowe ID: ");
                    x = false;
                }
            }
            while (x == false);
        }

            public static bool Details(List<Person> lista)
        {
                Console.WriteLine("\n----------------------------------------------------" +
                   "\n 1. Wyświetl szczegóły pozycji\n" +
                   " 2. Powrót do Menu");

                bool x = true;
                switch (Engine.WaitForKey('2'))
                {
                    case '1':
                        Console.WriteLine("\n\n Podaj ID pozycji: ");
                        do
                        {
                        string line = Console.ReadLine();
                            List<Person> person = lista.Where(x => x.Id == line).ToList();

                            if (person.Count == 1)
                            {
                                DetailsMenu();
                                Console.WriteLine($"\n {person[0].Id}| {person[0].LName} {person[0].FName}");
                                Console.Write("\n Numery telefonów: ");
                                foreach (PhoneNumber numer in person[0].PhoneList)
                                {
                                    Console.Write($"{numer.Kierunkowy} {numer.Numer}, ");
                                }

                                if (person[0].PhoneList.Count == 0)
                                    Console.Write("(brak numeru)");

                                Console.Write($"\n\n Adres: {person[0].Adres.PostCode} {person[0].Adres.City} , {person[0].Adres.Street} " +
                                    $"{person[0].Adres.HomeNumber}");

                                if (person[0].Adres.FlatNumber != "")
                                {
                                    Console.Write($"/{person[0].Adres.FlatNumber}");
                                }
                                
                                Console.WriteLine($"\n\n Stan cywilny: {person[0].Marital}");
                                Console.WriteLine("\n\n (Wciśnij dowolny klawisz aby wrócić do listy)");
                                Console.ReadKey();
                                x = true;
                            }
                            else
                            {
                                Console.WriteLine("\n !! Nieprawidłowa wartość. !! Podaj prawdiłowe ID: ");
                                x = false;
                            }
                        }
                        while (x == false);
                        break;
                    case '2':
                        x = false;
                        break;
                }
            return x;

            void DetailsMenu()
            {
                Console.Clear();
                Console.WriteLine("\n         KSIĄŻKA TELEFONICZNA");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("         |Szczegóły pozycji|");
                Console.WriteLine("----------------------------------------------------");
            }
        }
    }
}
