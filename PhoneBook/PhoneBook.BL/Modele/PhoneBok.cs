﻿using PhoneBook.UI;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace PhoneBookNamespace.BL
{
    public class PhoneBok : IPhoneBook
    {
        public List<Person> PersonList { get; set; }

        public void ShowAll()
        {
            Display.Menu1(PersonList);
        }

        public void Add(string bookpath, string idpath)
        {
            Display.Menu3(PersonList, bookpath, idpath);
        }

        public void Delete(string bookpath, string idpath)
        {
            Display.Menu5(PersonList, bookpath, idpath);
        }

        public void Edit(string bookpath)
        {
            Display.Menu4(PersonList, bookpath);
        }

        public void Search()
        {
            Display.Menu2(PersonList);
        }

    }
}
