﻿using PhoneBook.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBookNamespace.BL
{
    public class Person : IPerson
    {
        public string Id { get; private set; }
        public string LName { get; private set; }
        public string FName { get; private set; }
        public string Marital { get; private set; }
        public Adres Adres { get; private set; }
        public List<PhoneNumber> PhoneList { get; private set; }
        //static public string Fb;

        public Person(string id)
        {
            Id = id;
            LName = "(brak danych)";
            FName = "(brak danych)";
            Marital = "(brak danych)";
            Adres = new Adres();
            PhoneList = new List<PhoneNumber>();
        }

        public Person(string id, string lName, string fName, string marital, Adres adres, List<PhoneNumber> phonelist)
        {
            Id = id;
            LName = lName;
            FName = fName;
            Marital = marital;
            Adres = adres;
            PhoneList = phonelist;
        }

        public void SetLname(string lname)
        {
            LName = lname;
        }

        public void SetFname(string fname)
        {
            FName = fname;
        }

        public void SetMarital(string marital)
        {
            Marital = marital;
        }
    }
}
