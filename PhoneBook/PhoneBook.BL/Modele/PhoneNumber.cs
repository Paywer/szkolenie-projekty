﻿using PhoneBook.UI;
using System;

namespace PhoneBookNamespace.BL
{
    public class PhoneNumber : IPhoneNumber
    {
        public string Typ { get; private set; }
        public string Kierunkowy { get; private set; }
        public string Numer { get; private set; }

        public PhoneNumber()
        {
            Typ = "";
            Kierunkowy = "";
            Numer = "(brak numeru)";
        }

        public PhoneNumber(string typ, string kierunkowy, string numer)
        {
            Typ = typ;
            Kierunkowy = kierunkowy;
            Numer = numer;
        }

        public void SetTyp(string value)
        {
            Typ = value;
        }

        public void SetKierunkowy(string value)
        {
            Kierunkowy = value;
        }

        public void SetNumer(string value)
        {
            Numer = value;
        }
    }
}