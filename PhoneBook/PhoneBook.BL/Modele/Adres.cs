﻿using PhoneBook.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBookNamespace.BL
{
    public class Adres : IAdres
    {
        public string PostCode { get; private set; }
        public string City { get; private set; }
        public string Street { get; private set; }
        public string HomeNumber { get; private set; }
        public string FlatNumber { get; private set; }

        public Adres()
        {
            PostCode = "(brak danych)";
            City = "(brak danych)";
            Street = "(brak danych)";
            HomeNumber = "(brak danych)";
            FlatNumber = "";
        }

        public Adres(string postCode, string city, string street, string homeNumber, string flatNumber)
        {
            PostCode = postCode;
            City = city;
            Street = street;
            HomeNumber = homeNumber;
            FlatNumber = flatNumber;
        }

        public void SetPostCode(string value)
        {
            PostCode = value;
        }

        public void SetCity(string value)
        {
            City = value;
        }

        public void SetStreet(string value)
        {
            Street = value;
        }

        public void SetHomeNumber(string value)
        {
            HomeNumber = value;
        }

        public void SetFlatNumber(string value)
        {
            FlatNumber = value;
        }
    }
}
