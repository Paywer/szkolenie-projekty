﻿using System;
using System.Collections.Generic;

namespace DaysOfWeek
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", 
                "Friday",
                "Saturday", 
                "Sunday"};

            foreach (string day in daysOfWeek)
            {
                Console.WriteLine(day);
            }
             
            /*
            Console.WriteLine("Któr dzień chcesz wyświetlić?");
            Console.WriteLine("(Monday =1, etc.) > ");
            int iDay = int.Parse(Console.ReadLine());

            string chosenDay = daysOfWeek[iDay-1];
            Console.WriteLine($"That day is {chosenDay}");
            */   
    }
    }
}
